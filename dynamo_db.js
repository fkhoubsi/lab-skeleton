const logger = require('./logger'); 

require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  logger.info("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  // create a scan command to get all products
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  const randomIndex = Math.floor(Math.random() * response.Items.length);
  return response.Items[randomIndex];

};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  // create a scan command to get all products
  const command = new ScanCommand({
    TableName: "Products",
    FilterExpression: "category = :category",
    ExpressionAttributeValues: {
      ":category": category,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
  
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  // generate a random id for the order
  order.id = uuid.v4();
  const command = new PutCommand({
    TableName: "Orders",
    Item: order,
  });

  const response = await docClient.send(command);
  return response.Item;
};


const updateUser = async (id, updates) => {
  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: "set email = :email, password = :password",
    ExpressionAttributeValues: {
      ":email": updates.email,
      ":password": updates.password,
    },
    ReturnValues: "UPDATED_NEW",
  });

  const response = await docClient.send(command);
  return response;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
