require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();
const logger = require('./logger'); 

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  logger.info("MySql connected!");
};

const queryProductById = async (productId) => {
  const res= await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`);


  return res[0][0];
};

const queryRandomProduct = async () => {
  const [rows] = await mysql.query("SELECT * FROM products;");
  const randomIndex = Math.floor(Math.random() * rows.length);
  return rows[randomIndex];
};

const queryAllProducts = async () => {
  return (await mysql.query("SELECT * FROM products;"))[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

const insertOrder = async (order) => {
  // fetch order fields and create a random id
  const { user_id, products, total_amount } = order;
  const id = uuid.v4();

  await mysql.query(`INSERT INTO orders (id, user_id, total_amount)
                      VALUES ("${id}", "${user_id}", "${total_amount}");`);
  
  
  for (const product of products) {
    await mysql.query(`INSERT INTO order_items (id, order_id, product_id, quantity)
                        VALUES ("${uuid.v4()}", "${id}", "${product.product_id}", "${product.quantity}");`);
  }

  return "Success";
};

const updateUser = async (id, updates) => {
  // fetch the fields
  const { email, password, name} = updates;
  await mysql.query(`UPDATE users
                      SET email = "${email}", password = "${password}", name = "${name}"  
                      WHERE id = "${id}";`);
  return await queryUserById(id);
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
