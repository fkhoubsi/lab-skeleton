const logger = require('./logger'); // Assuming you have a logger
let requestCounter = 0;

const db = process.argv[3];

const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");

const logRequest = (endpoint, details) => {
  requestCounter++;
  logger.info(`Request #${requestCounter}: ${endpoint} - ${details}`);
};

const getRandomProduct = async (req, res) => {
  logRequest("getRandomProduct", "No Parameters");

  const randProd = await queryRandomProduct();
  res.send(randProd);
};

const getProduct = async (req, res) => {
  logRequest("getProduct", `Params: productId=${req.params.productId}`);

  const { productId } = req.params;
  const product = await queryProductById(productId);
  res.send(product);
};

const getProducts = async (req, res) => {
  logRequest("getProducts", `Query: category=${req.query.category || "None"}`);

  const { category } = req.query;
  const products = await queryAllProducts(category);
  res.send(products);
};

const getCategories = async (req, res) => {
  logRequest("getCategories", "No Parameters");

  const categories = await queryAllCategories();
  res.send(categories);
};

const getAllOrders = async (req, res) => {
  logRequest("getAllOrders", "No Parameters");

  const orders = await queryAllOrders();
  res.send(orders);
};

const getOrdersByUser = async (req, res) => {
  logRequest("getOrdersByUser", `Query: userId=${req.query.userId}`);

  const { userId } = req.query;
  const orders = await queryOrdersByUser(userId);
  res.send(orders);
};

const getOrder = async (req, res) => {
  logRequest("getOrder", `Params: orderId=${req.params.orderId}`);

  const { orderId } = req.params;
  const order = await queryOrderById(orderId);
  res.send(order);
};

const getUser = async (req, res) => {
  logRequest("getUser", `Params: userId=${req.params.userId}`);

  const { userId } = req.params;
  const user = await queryUserById(userId);
  res.send(user);
};

const getUsers = async (req, res) => {
  logRequest("getUsers", "No Parameters");

  const users = await queryAllUsers();
  res.send(users);
};

const postOrder = async (req, res) => {
  logRequest("postOrder", "Body: " + JSON.stringify(req.body));

  const user_id = req.body.user_id;
  const products = req.body.products;
  const total_amount = req.body.total_amount;

  const order = await insertOrder({
    user_id,
    products,
    total_amount,
  });

  res.send(order);
};

const patchUser = async (req, res) => {
  logRequest("patchUser", `Params: userId=${req.params.userId}, Body: ` + JSON.stringify(req.body));

  const updates = req.body;
  const { userId } = req.params;
  const response = await updateUser(userId, updates);
  res.send(response);
};

module.exports = {
  init,
  getProduct,
  getRandomProduct,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getProducts,
  getUser,
  getUsers,
  postOrder,
  patchUser,
};
